package com.cs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Contact Already Exist")
public class ContactAlreadyExistException extends Exception{

	private static final long serialVersionUID = 1L;

	public ContactAlreadyExistException(String errorMessage) {
		super(errorMessage);
	}

	

}
