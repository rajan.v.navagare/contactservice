package com.cs.model;

import lombok.Data;

@Data
public class Address {

	private Integer id;

	private String userId;

	private Long address;

	private String city;

	private Long state;

	private String country;

}
