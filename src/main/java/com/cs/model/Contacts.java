package com.cs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
public class Contacts {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Exclude
	private int id;
	
	@EqualsAndHashCode.Include
	private String userId;
	
	@EqualsAndHashCode.Include
	private long mobileNo;
	
	@EqualsAndHashCode.Include
	private String email;
}
