package com.cs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cs.model.Contacts;
import com.cs.services.ContactServices;

@RestController
public class ContactsPatchController {

	@Autowired
	ContactServices contactServices;

	@PatchMapping(value = "/updateContact")
	public ResponseEntity<Contacts> updateContacts(@RequestBody Contacts contacts) {
		System.out.println(contacts);
		Contacts updatedRecord = contactServices.updateContact(contacts);
		return new ResponseEntity<Contacts>(updatedRecord, HttpStatus.OK);
	}

}
