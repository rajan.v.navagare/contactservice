package com.cs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cs.services.ContactServices;

@RestController
public class ContactsDeleteController {

	@Autowired
	ContactServices contactServices;

	@Autowired
	ContactsGetController contactsGetController;

	@DeleteMapping("/deleteContactById")
	public int deleteContacts(int id) {
		int recordDeleted = 0;
		contactServices.deleteContact(id);
		return recordDeleted;
	}

	@DeleteMapping("/deleteContactsByUserId")
	public int deleteContactsByUser(@RequestParam(required = true) String userId) {
		System.out.println("delete called with:" + userId);
		int numberOfContactsDeleted = 0;
		if (contactsGetController.getNumberOfContacts(userId) > 0) {
			numberOfContactsDeleted = contactServices.deleteAllByUserId(userId);
		}
		return numberOfContactsDeleted;
	}

}
