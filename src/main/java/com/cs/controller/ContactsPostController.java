package com.cs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cs.exceptions.ContactAlreadyExistException;
import com.cs.model.Contacts;
import com.cs.services.ContactServices;

@RestController
public class ContactsPostController {

	@Autowired
	ContactServices contactServices;

	@Autowired
	ContactsGetController contactsGetController;

	@PostMapping("/addContact")
	public ResponseEntity<Contacts> addContacts(@RequestBody Contacts contacts) throws ContactAlreadyExistException {
		Contacts reutnedContact = null;
		List<Contacts> listOfContacts = contactServices.getContactsByUser(contacts.getUserId());
		listOfContacts.contains(contacts);
		System.out.println("op:" + listOfContacts.contains(contacts));

		if (listOfContacts.contains(contacts)) {
			throw new ContactAlreadyExistException("contacts:" + contacts);
		} else {
			reutnedContact = contactServices.addContact(contacts);

		}
		return new ResponseEntity<Contacts>(reutnedContact, HttpStatus.CREATED);
	}

}
