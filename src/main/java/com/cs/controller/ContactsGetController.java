package com.cs.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cs.model.Contacts;
import com.cs.services.ContactServices;

@RestController
@RequestMapping("/get")
public class ContactsGetController {

	@Autowired
	ContactServices contactServices;
	
	//To handle wrong URLS
	@GetMapping("/*")
	public ResponseEntity<String> wrongUrlHandling() {
		System.out.println("Wrong Url please enter correct urls");
		//hateoas can be used here to show different available URLS.
		return new ResponseEntity<String>("Wrong Url please enter correct urls", HttpStatus.NOT_FOUND);
	}
	
	//this is main service
	@GetMapping("/getContactsByUserId/{userId}")
	public ResponseEntity<List<Contacts>> getContactsByUser(@PathVariable(required = true) String userId) {
		System.out.println("called with:" + userId);
		List<Contacts> contactsList = contactServices.getContactsByUser(userId);
		if (contactsList.isEmpty())
			return new ResponseEntity<List<Contacts>>(contactsList, HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<List<Contacts>>(contactsList, HttpStatus.OK);
	}
	
	//This is main service with different form
	@GetMapping("/getContactsByUserId")
	public ResponseEntity<List<Contacts>> getContactsByUser2(@RequestParam String userId) {
		System.out.println("called with:" + userId);
		List<Contacts> contactsList = contactServices.getContactsByUser(userId);
		if (contactsList.isEmpty())
			return new ResponseEntity<List<Contacts>>(contactsList, HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<List<Contacts>>(contactsList, HttpStatus.OK);
	}
	
	//to get all contacts as list
	@GetMapping("/getAllContacts")
	public ResponseEntity<List<Contacts>> getAll() {
		List<Contacts> contactsList = contactServices.getAll();
		return ResponseEntity.ok(contactsList);
	}
	
	//Below is another way for sending status code as a default status code
	@GetMapping("/getAllContacts1")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Contacts> getAll1() {
		List<Contacts> contactsList = contactServices.getAll();
		return contactsList;
	}
	
	//get contacts by ID Mostly used internally as user wont come to know about ID
	@GetMapping(value = "/getContactById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Contacts> getContact(int id) {
		Optional<Contacts> optionalContact = contactServices.getContact(id);

		if (optionalContact.isPresent())
			return new ResponseEntity<Contacts>(optionalContact.get(), HttpStatus.OK);
		else
			return new ResponseEntity<Contacts>(new Contacts(), HttpStatus.NO_CONTENT);
	}
	
	//get contacts count for a particular user ID
	@GetMapping(value = "/getContactsCountByUserId", produces = MediaType.APPLICATION_JSON_VALUE)
	public int getNumberOfContacts(String userId) {
		List<Contacts> userContacts = contactServices.getContactsByUser(userId);
		int numberOfContacts = userContacts.size();
		return numberOfContacts;
	}

}
