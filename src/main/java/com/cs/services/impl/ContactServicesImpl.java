package com.cs.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.ContactsDao;
import com.cs.model.Contacts;
import com.cs.services.ContactServices;

@Transactional
@Service
public class ContactServicesImpl implements ContactServices {

	@Autowired
	ContactsDao<Contacts> contactsDao;

	@Override
	public List<Contacts> getAll() {
		List<Contacts> contactsList = contactsDao.findAll();
		return contactsList;
	}

	@Override
	public Optional<Contacts> getContact(int id) {
		Optional<Contacts> contact = contactsDao.findById(id);
		return contact;
	}

	@Override
	public List<Contacts> getContactsByUser(String userId) {
		System.out.println("userIDs:" + userId);
		List<Contacts> contacts = contactsDao.findByUserId(userId);
		return contacts;
	}

	@Override
	public Contacts addContact(Contacts contacts) {
		contactsDao.equals(contacts);
		Contacts returnedContact = contactsDao.save(contacts);
		return returnedContact;
	}

	@Override
	public Contacts updateContact(Contacts contacts) {
		Contacts returnedContact = contactsDao.save(contacts);
		return returnedContact;
	}

	@Override
	public void deleteContact(int id) {
		contactsDao.deleteById(id);;
	}

	@Override
	public int deleteAllByUserId(String userId) {
		int deleteCount = contactsDao.deleteAllByUserId(userId);
		return deleteCount;
	}

}
