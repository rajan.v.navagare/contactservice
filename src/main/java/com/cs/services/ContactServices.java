package com.cs.services;

import java.util.List;
import java.util.Optional;

import com.cs.model.Contacts;

public interface ContactServices {

	public List<Contacts> getAll();

	public Contacts addContact(Contacts contacts);

	public Contacts updateContact(Contacts contacts);

	public void deleteContact(int id);

	public Optional<Contacts> getContact(int id);

	public List<Contacts> getContactsByUser(String userId);

	public int deleteAllByUserId(String userId);

}
