package com.cs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cs.model.Contacts;


@Repository
public interface ContactsDao<t> extends JpaRepository<Contacts , Integer> {

	public List<Contacts> findByUserId(String userId);

	public int deleteAllByUserId(String userId);

}
